import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';



export default class App extends React.Component {
  constructor () {
    super ();
    this.state = {
      resultSum: '',
      calculationSum: ''
    };

    this.operations = ['DEL', '+', '-', '*', '/'];
  }

  calculateResult() {
    const button = this.state.resultSum;
    this.setState({calculationSum: eval(button)});
  }

  validate() {
    const button = this.state.resultSum;
    switch (button.slice(-1)) {
      case '+':
      case '-':
      case '*':
      case '/':
        return false;
    }
    return true;
  }

  buttonPressed (button) {
    if (button === '=') {
      return  this.validate() && this.calculateResult()
    }
    this.setState({
      resultSum: this.state.resultSum + button
    })
  }

  operate(operation) {
    switch(operation) {
      case 'DEL':
        let text = this.state.resultSum.split('');
        text.pop();
        this.setState({resultSum: text.join('')});
        break;
      case '+':
      case '-':
      case '*':
      case '/':
        const lastChar = this.state.resultSum.split('').pop();
        if (this.operations.indexOf(lastChar) > 0) return;
        if (this.state.text === '') return;
        this.setState({resultSum: this.state.resultSum + operation});
    }
  }
  render() {
    let rows = [];
    let nums = [[1, 2, 3], [4, 5, 6], [7, 8, 9], ['.', 0, '=']];
    for (let i = 0; i < 4; i++) {
      let row = [];
      for (let j = 0; j < 3; j++) {
        row.push(
            <TouchableOpacity
                key={nums[i][j]}
                style={styles.btn}
                onPress={() => this.buttonPressed(nums[i][j])}
            >
              <Text style={styles.btnText}>
                {nums[i][j]}
              </Text>
            </TouchableOpacity>)
      }
      rows.push(<View style={styles.row} key={nums[i]}>{row}</View>)
    }

    let ops = [];
    for (let i = 0; i < 5; i++) {
      ops.push(
          <TouchableOpacity
              key={this.operations[i]}
              style={styles.btn}
              onPress={() => this.operate(this.operations[i])}
          >
            <Text style={styles.btnText}>
              {this.operations[i]}
            </Text>
          </TouchableOpacity>)
    }
    return (
      <View style={styles.container}>
        <View style={styles.displayResults}>
          <Text style={styles.results}>{this.state.resultSum}</Text>
        </View>
        <View style={styles.calculation}>
          <Text style={styles.calculationResult}>{this.state.calculationSum}</Text>
        </View>
        <View style={styles.buttons}>
          <View style={styles.numbers}>
            {rows}
          </View>
          <View style={styles.operations}>
            {ops}
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  results: {
    fontSize: 20,
  },
  calculationResult: {
    fontSize: 14
  },
  row: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  displayResults: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  calculation: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  buttons: {
    flexGrow: 7,
    flexDirection: 'row'
  },
  btn: {
    flex: 1,
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  btnText: {
    fontSize: 30,
    color: '#fff'
  },
  numbers: {
    flex: 3,
    backgroundColor: '#434343',
    color: '#fff'
  },
  operations: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'stretch',
    backgroundColor: '#636363'
  }
});
